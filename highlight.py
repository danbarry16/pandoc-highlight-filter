#!/usr/bin/env python
# -*- coding: utf-8 -*-

from contextlib import redirect_stdout
from io import StringIO
from pandocfilters import toJSONFilter, RawBlock
import re
import signal
import sys
import traceback

# Global
exec_globals = {}
line_counter = 0

# Highlight
#
# Process the code sections as appropriate and highlight where required.
class Highlight:
  exec_timeout = 10 # Seconds

  symbolother_col  = "#FF920D"
  symbolmath_col   = "#197BCE"
  symbolnumber_col = "#E32929"
  symbolpairs_col  = "#AF18DB"
  string_col       = "#008000"
  comment_col      = "#888888"
  line_col         = "#444444"

  fbeg = '<font color="'
  fend = '</font>'

  fontbeg = '<b>' + fbeg
  fontend = fend + '</b>'

  escape = {
    "&" : "&amp;",
    "<" : "&lt;",
    ">" : "&gt;",
  }

  symbols = {
    # symbolother
    "&"  : fontbeg + symbolother_col  + '">&amp;'  + fontend,
    "."  : fontbeg + symbolother_col  + '">.'      + fontend,
    ","  : fontbeg + symbolother_col  + '">,'      + fontend,
    "?"  : fontbeg + symbolother_col  + '">?'      + fontend,
    "!"  : fontbeg + symbolother_col  + '">!'      + fontend,
    "£"  : fontbeg + symbolother_col  + '">&#163;' + fontend,
    "$"  : fontbeg + symbolother_col  + '">$'      + fontend,
    "%"  : fontbeg + symbolother_col  + '">%'      + fontend,
    "@"  : fontbeg + symbolother_col  + '">@'      + fontend,
    "~"  : fontbeg + symbolother_col  + '">~'      + fontend,
    "|"  : fontbeg + symbolother_col  + '">|'      + fontend,
    "#"  : fontbeg + symbolother_col  + '">#'      + fontend,
    ":"  : fontbeg + symbolother_col  + '">:'      + fontend,
    ";"  : fontbeg + symbolother_col  + '">;'      + fontend,
    "="  : fontbeg + symbolother_col  + '">='      + fontend,
    "_"  : fontbeg + symbolother_col  + '">_'      + fontend,
    "\"" : fontbeg + symbolother_col  + '">&#34;'  + fontend,
    "\'" : fontbeg + symbolother_col  + '">\''     + fontend,
    "<"  : fontbeg + symbolother_col  + '">&lt;'   + fontend,
    ">"  : fontbeg + symbolother_col  + '">&gt;'   + fontend,
    # symbolpairs
    "["  : fontbeg + symbolpairs_col  + '">['      + fontend,
    "]"  : fontbeg + symbolpairs_col  + '">]'      + fontend,
    "{"  : fontbeg + symbolpairs_col  + '">{'      + fontend,
    "}"  : fontbeg + symbolpairs_col  + '">}'      + fontend,
    "("  : fontbeg + symbolpairs_col  + '">('      + fontend,
    ")"  : fontbeg + symbolpairs_col  + '">)'      + fontend,
    # symbolmath
    "+"  : fontbeg + symbolmath_col   + '">+'      + fontend,
    "-"  : fontbeg + symbolmath_col   + '">-'      + fontend,
    "*"  : fontbeg + symbolmath_col   + '">*'      + fontend,
    "^"  : fontbeg + symbolmath_col   + '">^'      + fontend,
    "/"  : fontbeg + symbolmath_col   + '">/'      + fontend,
    "\\" : fontbeg + symbolmath_col   + '">\\'     + fontend,
    # symbolnumber
    "0"  : fontbeg + symbolnumber_col + '">0'      + fontend,
    "1"  : fontbeg + symbolnumber_col + '">1'      + fontend,
    "2"  : fontbeg + symbolnumber_col + '">2'      + fontend,
    "3"  : fontbeg + symbolnumber_col + '">3'      + fontend,
    "4"  : fontbeg + symbolnumber_col + '">4'      + fontend,
    "5"  : fontbeg + symbolnumber_col + '">5'      + fontend,
    "6"  : fontbeg + symbolnumber_col + '">6'      + fontend,
    "7"  : fontbeg + symbolnumber_col + '">7'      + fontend,
    "8"  : fontbeg + symbolnumber_col + '">8'      + fontend,
    "9"  : fontbeg + symbolnumber_col + '">9'      + fontend,
  }

  asm_family = [
    ["\"", "\"", string_col,  '\\', '\\'],
    ["'",  "'",  string_col,  '\\', '\\'],
    [";",  "\n", comment_col, '\0', '\0'],
  ]

  c_family = [
    ["\"", "\"", string_col,  '\\', '\\'],
    ["'",  "'",  string_col,  '\\', '\\'],
    ["//", "\n", comment_col, '\0', '\0'],
    ["/*", "*/", comment_col, '\0', '\0'],
  ]

  script_family = [
    ["\"", "\"", string_col,  '\\', '\\'],
    ["'",  "'",  string_col,  '\\', '\\'],
    ["#",  "\n", comment_col, '\0', '\0'],
  ]

  # NOTE: This will fail on some strings.
  viml = [
    ["\"", "\n", comment_col, '\0', '\0'],
    ["\"",  "\"", string_col,  '\\', '\\'],
  ]

  # __init__()
  #
  # Initialise the highlighter.
  #
  # @param self Reference to self.
  # @param key The type of data it is as indicated by the markup.
  # @param value The payload of the data (unicode).
  # @param format ?
  # @param meta That dumb company Facebook turned into.
  # @return The new data structure, otherwise don't return anything and the
  # default is used.
  def __init__(self, key, value, format, meta) :
    self.format = None
    self.lang = None
    self.output = None
    if key == "CodeBlock" :
      if format == "html" :
        self.format = format
      self.lang = ""
      # Attempt pulling out the language label
      try :
        self.lang = value[0][1][0]
      except Exception as e :
        self.lang = ""
      # Check if we need to execute the code
      if self.lang == "python-exec" :
        self.process("try:from plott import Plott\nexcept:pass", self.exec_timeout)
        redirect_func = StringIO()
        with redirect_stdout(redirect_func):
          self.process(value[1], self.exec_timeout)
        self.output = redirect_func.getvalue()
    return

  # process_lines()
  #
  # Process the lines based on the language.
  # @param self Reference to self.
  # @param value The data provided to the filter.
  def process_lines(self, value) :
    if self.format == None or self.lang == None :
      return
    # Markup the labels to look pretty
    if(self.lang == "c"          or
       self.lang == "cpp"        or
       self.lang == "c++"        or
       self.lang == "c#"         or
       self.lang == "java"       or
       self.lang == "javascript" or
       self.lang == "js"         or
       self.lang == "groovy"     or
       self.lang == "perl"       or
       self.lang == "php") :
      value[1] = self.lang_markup(value[1], self.symbols, self.escape, self.c_family)
    elif(self.lang == "bash"   or
         self.lang == "sh"     or
         self.lang == "ksh"    or
         self.lang == "csh"    or
         self.lang == "python" or
         self.lang == "python-exec") :
      value[1] = self.lang_markup(value[1], self.symbols, self.escape, self.script_family)
    elif(self.lang == "asm"  or
         self.lang == "fasm" or
         self.lang == "nasm") :
      value[1] = self.lang_markup(value[1], self.symbols, self.escape, self.asm_family)
    elif(self.lang == "vim"   or
         self.lang == "vimrc" or
         self.lang == "viml") :
      value[1] = self.lang_markup(value[1], self.symbols, self.escape, self.viml)
    else :
      value[1] = self.multiple_replace(value[1], self.symbols)
    self.code = value[1]

  # get_output()
  #
  # Get the marked output from the highlight filter.
  #
  # @param self Reference to self.
  # @return The marked output from the highlight filter.
  def get_output(self) :
    if self.format == None or self.lang == None :
      return None
    code_block = self.line_markup(self.code)
    if self.output == None :
      return RawBlock("html", "<pre>" + code_block + "</pre>")
    else :
      return RawBlock("html", "<pre>" + code_block + "</pre><pre class=\"python-out\">" + data_uri(self.multiple_replace(self.output, self.escape)) + "</pre>")

  # eprint()
  #
  # Print to standard error as not to interfere with parsing.
  #
  # @param self Reference to self.
  # @param s The string to be printed.
  def eprint(self, s) :
    sys.stderr.write(s + "\n")
    return

  # handler()
  #
  # Register a handler for the timeout.
  #
  # @param self Reference to self.
  # @param signum The signal number.
  # @param frame The frame of the timeout.
  # @throws Exception Timeout.
  def handler(self, signum, frame) :
    raise Exception("Computation timeout")
    return

  # is_unix()
  #
  # Determine whether the current system is Unix-based. In this case we test for
  # Linux and Darwin platforms.
  #
  # @param self Reference to self.
  # @return True if Unix, otherwise False.
  def is_unix(self) :
    return sys.platform == "linux" or sys.platform == "linux2" or platform == "darwin"

  # process()
  #
  # Process a given command and attempt to provide safeguards against the main
  # system, including timeouts.
  #
  # @param self Reference to self.
  # @param command The command to be run.
  # @param timeout The maximum time a process can be run (Unix only).
  def process(self, command, timeout) :
    global exec_globals
    # Only attempt use of signals if they exist
    if self.is_unix() :
      signal.signal(signal.SIGALRM, self.handler)
      signal.alarm(timeout)
    try :
      exec(command, exec_globals)
    except Exception as e :
      # TODO: Hide absolute path for security reasons.
      print(traceback.format_exc())
    if self.is_unix() :
      signal.alarm(0)
    return

  # lang_markup()
  #
  # Provide basic language markup.
  #
  # @param self Reference to self.
  # @param string The string to be parsed.
  # @param rep_dict The replace dictionary.
  # @param esc_dict The escape dictionary.
  # @param lang_lut The language specific markup to be performed.
  # @return The modified string.
  def lang_markup(self, string, rep_dict, esc_dict, lang_lut) :
    string = self.break_up(string, lang_lut)
    result = ""
    for s in string :
      updated = False
      for r in lang_lut :
        if s.startswith(r[0]) :
          result += self.fbeg + r[2] + '">' + self.multiple_replace(s, esc_dict) + self.fend
          updated = True
          break
      if not updated :
        result += self.multiple_replace(s, rep_dict)
    return result

  # break_up()
  #
  # Break up a given string into parts defined by start and end strings.
  #
  # NOTE: The escaping performed here is no bulletproof and is relatively dumb.
  # It will fail at some edge cases, such as: "\\"
  #
  # @param self Reference to self.
  # @param string The string to be broken up.
  # @param pairs The start string, end string, <ignore>,
  def break_up(self, string, pairs, start = 0) :
    s_beg = -1
    s_end = -1
    for p in pairs :
      # Do we beet our current best end?
      s_tmp = string.find(p[0], start)
      if s_tmp >= start and (s_tmp < s_beg or s_beg < 0) :
        # Ensure we don't meet an escape character
        if s_tmp > 0 and string[s_tmp - 1] == p[3] :
          continue
        s_beg = s_tmp
        # Try to find an end point
        s_end = string.find(p[1], s_beg + 1)
        if s_end >= start :
          while string[s_end - 1] == p[4] :
            s_end = string.find(p[1], s_end + 1)
            if s_end < 0 :
              s_end = len(string)
              break
          else :
            s_end += len(p[1])
        else :
          s_end = len(string)
    if s_beg < start or s_end < start :
      return [string[start:len(string)]]
    else :
      result = [string[start:s_beg], string[s_beg:s_end]]
      result += self.break_up(string, pairs, s_end)
      return result

  # line_markup()
  #
  # Generate line numbers with HTML links that can be referenced.
  #
  # @param self Reference to self.
  # @param string The string to be processed.
  # @return The string with the added HTML line numbers.
  def line_markup(self, string) :
    global line_counter
    lines = string.split("\n")
    string = ""
    for x in range(len(lines)) :
      line_counter += 1
      if x > 0 :
        string += "\n"
      string += (
        '<a class="disable" id="L' + str(line_counter) + '" href="#L' + str(line_counter) + '">' +
        self.fbeg + self.line_col + '">' + str(line_counter).rjust(4, '0') + self.fend +
        '</a>&nbsp;' + lines[x]
      )
    return string

  # Source: https://stackoverflow.com/a/15448887/2847743
  def multiple_replace(self, string, rep_dict):
    pattern = re.compile("|".join([re.escape(k) for k in sorted(rep_dict,key=len,reverse=True)]), flags=re.DOTALL)
    return pattern.sub(lambda x: rep_dict[x.group(0)], string)

# highlight()
#
# Filter for code blocks and perform the highlighting.
#
# @param key The type of data it is as indicated by the markup.
# @param value The payload of the data (unicode).
# @param format ?
# @param meta That dumb company Facebook turned into.
# @return The new data structure, otherwise don't return anything and the
# default is used.
def highlight(key, value, format, meta) :
  hl = Highlight(key, value, format, meta)
  hl.process_lines(value)
  return hl.get_output()

# data_uri()
#
# Parse each line and chek for the existance of a Data URI.
#
# @param s The string to be processed.
# @return The parsed data.
def data_uri(s) :
  output = ""
  lines = s.split("\n")
  for x in range(len(lines)) :
    # TODO: Better checking for Data URIs.
    if lines[x].startswith("data:") :
      lines[x] = "<img src=\"" + lines[x] + "\" />"
    if x > 0 :
      output += "\n"
    output += lines[x]
  return output

# Main entry into the program
if __name__ == "__main__" :
  toJSONFilter(highlight)
