#!/usr/bin/env python
# -*- coding: utf-8 -*-

import base64

# Plott
#
# A very simple Python plotting software.
#
# Why not use ...? This library aims to be small, lightweight and use
# little RAM. If you need additional features, look at using a more
# heavyweight lbrary. If you need to just visually compare some
# numbers cheaply, this is the library for you!
class Plott :
  # __init__()
  #
  # Initialise the plot.
  #
  # @param self Reference to self.
  # @param title The title of the graph.
  # @param x_title The X axis title.
  # @param y_title The Y axis title.
  # @param border The size of the border to be used.
  # @param width The width of the plot.
  # @param height The height of the plot.
  # @param fg The foreground colour.
  # @param bg The background colour.
  # @return Reference to self.
  def __init__(self, title = None, x_title = None, y_title = None, border = 32, width = 640, height = 480, fg = "#000", bg = "#FFF") :
    # Save internal variables
    self.title = title
    self.x_title = x_title
    self.y_title = y_title
    self.border = border
    self.width = width
    self.height = height
    self.fg = fg
    self.bg = bg
    # Error message for plotting
    self.error = None
    # Empty array of plots
    self.plots = []
    # Axis limits
    self.x_min = None 
    self.x_max = None
    self.y_min = None
    self.y_max = None
    return

  # plot()
  #
  # Plot on the axis.
  #
  # @param self Reference to self.
  # @param x_data The X data to be plotted.
  # @param y_data The Y data to be plotted.
  # @param name The name of the plot. If None, name auto generated.
  # @param colour The colour of the plot. If set to None, a colour is picked
  # automatically.
  # @return Reference to self.
  def plot(self, x_data, y_data, name = None, colour = None) :
    desc = {}
    x_num = True
    y_num = True
    # Make sure the data is given
    if x_data == None or y_data == None or len(x_data) != len(y_data) :
      self.error = "Bad X or Y data, must be equal"
      return self
    # Check the data itself
    for i in range(len(x_data)) :
      if x_data[i] == None :
        self.error = "X data None found"
        return self
      if y_data[i] == None :
        self.error = "Y data None found"
        return self
      # Attempt cast X
      try :
        x_data[i] = float(x_data[i])
      except :
        x_num = False
      # Attempt cast Y
      try :
        y_data[i] = float(y_data[i])
      except :
        y_num = False
      # Check for min X
      if x_num and (self.x_min == None or x_data[i] < self.x_min) :
        self.x_min = x_data[i]
      # Check for min Y
      if y_num and (self.y_min == None or y_data[i] < self.y_min) :
        self.y_min = y_data[i]
      # Check for max X
      if x_num and (self.x_max == None or x_data[i] > self.x_max) :
        self.x_max = x_data[i]
      # Check for max Y
      if y_num and (self.y_max == None or y_data[i] > self.y_max) :
        self.y_max = y_data[i]
    # Add the data if we got this far
    desc["x_data"] = x_data
    desc["y_data"] = y_data
    # Perform bar chart conversion if needed
    if not x_num or not y_num :
      # Generate new plots
      for i in range(len(x_data)) :
        x = [ x_data[i] ] * 2
        y = [ y_data[i] ] * 2
        n = name
        if not x_num :
          n = x_data[i]
          x = [ i + 0.5 ] * 2
          y[1] = 0.0
        if not y_num :
          n = y_data[i]
          y = [ i + 0.5 ] * 2
          x[1] = 0.0
        self.plot(x, y, name = n, colour = colour)
      # Bin this plot
      return self
    # If name not given, generate one
    if name == None :
      name = "plot_" + str(len(self.plots))
    desc["name"] = name
    # If colour not given, generate one
    if colour == None :
      colour = self.get_col(len(self.plots))
    desc["colour"] = colour
    # Finally, add the plot
    self.plots += [ desc ]
    return self

  # to_svg()
  #
  # Convert the output to SVG string.
  #
  # @param self Reference to self.
  # @return The resulting SVG string.
  def to_svg(self) :
    img_width = self.width + (self.border * 2)
    img_height = self.height + (self.border * 2)
    legend_height = self.border * len(self.plots)
    # Header
    result = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>'
    result += '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">'
    result += '<svg width="' + str(img_width) + '" height="' + str(img_height + legend_height) + '" viewBox="0 0 ' + str(img_width) + ' ' + str(img_height + legend_height) + '" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">'
    # Background
    result += '<rect fill="' + self.bg + '" x="0" y="0" width="' + str(img_width) + '" height="' + str(img_height + legend_height) + '"/>'
    # Draw axis
    result += '<line x1="' + str(self.border) + '" y1="' + str(self.border) + '" x2="' + str(self.border) + '" y2="' + str(self.height + (self.border * 1.25)) + '" style="stroke:' + self.fg + ';stroke-width:2;" />'
    result += '<line x1="' + str(self.width + self.border) + '" y1="' + str(self.height + self.border) + '" x2="' + str(self.border * 0.75) + '" y2="' + str(self.height + self.border) + '" style="stroke:' + self.fg + ';stroke-width:2;" />'
    result += '<line x1="' + str(self.width + self.border) + '" y1="' + str(self.height + self.border) + '" x2="' + str(self.width + self.border) + '" y2="' + str(self.height + (self.border * 1.25)) + '" style="stroke:' + self.fg + ';stroke-width:2;" />'
    result += '<line x1="' + str(self.border) + '" y1="' + str(self.border) + '" x2="' + str(self.border * 0.75) + '" y2="' + str(self.border) + '" style="stroke:' + self.fg + ';stroke-width:2;" />'
    # Draw data points (points + joins)
    for plot in self.plots :
      prev_xy = None
      for i in range(len(plot["x_data"])) :
        x = plot["x_data"][i]
        y = plot["y_data"][i]
        # Normalise data point
        norm_x = x - self.x_min
        if self.x_max != self.x_min : norm_x = (norm_x / (self.x_max - self.x_min))
        x = (norm_x * self.width) + self.border
        norm_y = y - self.y_min
        if self.y_max != self.y_min : norm_y = (norm_y / (self.y_max - self.y_min))
        y = (self.height + self.border) - (norm_y * self.height)
        # Add point
        result += '<rect fill="' + plot["colour"] + '" x="' + str(x - 4) + '" y="' + str(y - 4) + '" width="8" height="8" />'
        # Join points
        if prev_xy != None :
          result += '<line x1="' + str(prev_xy[0]) + '" y1="' + str(prev_xy[1]) + '" x2="' + str(x) + '" y2="' + str(y) + '" style="stroke:' + plot["colour"] + ';stroke-width:2" />'
        # Save previous point
        prev_xy = [ x, y ]
    # Draw axis text
    result += '<text x="15%" y="' + str(img_height - (self.border / 2)) + '" dominant-baseline="middle" text-anchor="middle" fill="' + self.fg + '">' + "{0:.2E}".format(self.x_min) + '</text>'
    result += '<text x="85%" y="' + str(img_height - (self.border / 2)) + '" dominant-baseline="middle" text-anchor="middle" fill="' + self.fg + '">' + "{0:.2E}".format(self.x_max) + '</text>'
    result += '<text y="' + str(-(img_width - img_height - self.border) / 2) + '" x="25%" dominant-baseline="middle" text-anchor="middle" transform="rotate(-90 ' + str(img_width / 2) + ' ' + str(img_height / 2) + ')" fill="' + self.fg + '">' + "{0:.2E}".format(self.y_min) + '</text>'
    result += '<text y="' + str(-(img_width - img_height - self.border) / 2) + '" x="75%" dominant-baseline="middle" text-anchor="middle" transform="rotate(-90 ' + str(img_width / 2) + ' ' + str(img_height / 2) + ')" fill="' + self.fg + '">' + "{0:.2E}".format(self.y_max) + '</text>'
    # Draw titles
    if self.title != None :
      result += '<text x="50%" y="' + str(self.border / 2) + '" dominant-baseline="middle" text-anchor="middle" fill="' + self.fg + '">' + self.title + '</text>'
    if self.x_title != None :
      result += '<text x="50%" y="' + str(img_height - (self.border / 2)) + '" dominant-baseline="middle" text-anchor="middle" fill="' + self.fg + '">' + self.x_title + '</text>'
    if self.y_title != None :
      result += '<text y="' + str(-(img_width - img_height - self.border) / 2) + '" x="50%" dominant-baseline="middle" text-anchor="middle" transform="rotate(-90 ' + str(img_width / 2) + ' ' + str(img_height / 2) + ')" fill="' + self.fg + '">' + self.y_title + '</text>'
    # Draw legend
    for i in range(len(self.plots)) :
      x = self.border
      y = img_height + (self.border * (i + 0.25))
      result += '<rect fill="' + self.plots[i]["colour"] + '" x="' + str(x - 4) + '" y="' + str(y - 4) + '" width="8" height="8" />'
      result += '<text x="' + str(x + self.border) + '" y="' + str(y + (self.border * 0.25)) + '" fill="' + self.fg + '">' + self.plots[i]["name"] + '</text>'
    # Draw error message
    if self.error != None :
      result += '<text x="50%" y="50%" dominant-baseline="middle" text-anchor="middle" fill="' + self.fg + '">' + self.error + '</text>'
    # Footer
    result += '</svg>'
    return result

  # to_uri()
  #
  # Produce an SVG URI from the plot.
  #
  # @param self Reference to self.
  # @return The resulting SVG string.
  def to_uri(self) :
    svg = self.to_svg()
    svg_enc = base64.b64encode(svg.encode('utf-8'))
    uri = "data:image/svg+xml;base64," + svg_enc.decode("utf-8")
    return uri

  # Source: https://stackoverflow.com/a/26856771
  def hsv_to_rgb(self, h, s, v, a) :
    if s :
      if h == 1.0 :
        h = 0.0
      i = int(h * 6.0)
      f = h * 6.0 - i
      w = v * (1.0 - s)
      q = v * (1.0 - s * f)
      t = v * (1.0 - s * (1.0 - f))
      if i == 0:
        return [v, t, w, a]
      if i == 1:
        return [q, v, w, a]
      if i == 2:
        return [w, v, t, a]
      if i == 3:
        return [w, q, v, a]
      if i == 4:
        return [t, w, v, a]
      if i == 5:
        return [v, w, q, a]
    else :
      return [v, v, v, a]

  # get_col()
  #
  # Get a HTML RGB colour based on an index number.
  #
  # @param self Reference to self.
  # @param i The index number.
  # @return The suggested HTML colour.
  def get_col(self, i) :
    # hsv_to_rgb() function takes values between 0.0 and 1.0
    # Multiply the index value by golden angle number and mod it
    rgb = self.hsv_to_rgb((i * 0.381966) % 1.0, 1.0, 1.0, 1.0)
    # hsv_to_rgb() returns an array of 3 values between 0.0 and 1.0
    # Generate RGB bytes
    r = int(rgb[0] * 255)
    g = int(rgb[1] * 255)
    b = int(rgb[2] * 255)
    # Now put into a single integer representing the colour
    c = (r << 16) | (g << 8) | b
    # Convert to a hex string and remove '0x' from the front
    r = hex(c)[2:]
    # Ensure we end up with 6 characters
    while len(r) < 6 :
      r = "0" + r
    # Throw a '#' on the front to indicate it is a HTML colour
    return "#" + r
