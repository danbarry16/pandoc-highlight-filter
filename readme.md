**Pandoc Highlight Filter**

A quick and dirty highlight filter for Pandoc.

![Chromium light mode](doc/example-chromium-light.jpg)

![Firefox dark mode](doc/example-firefox-dark.jpg)

Features:

* Mostly language agnostic syntax highlighting.
* Adds line numbers with anchors.
* 'Special' formatting for strings and comments in some languages (feel free to
add some).
* Runs Python (**unsafe**, not sandboxed) that appears like a pre-processed
Jupyter Notebook.

(Known) Bugs:

* String escaping is really simple, there are some edge cases that would
require some time to work through, but this works for 99.99% of use cases.
* It could parse faster, but it's significantly faster than Pandoc is in most
cases, so there is little to be gained here.
* Security of `python-exec` is *minimal*, only run this on code you trust! Much
safer alternatives exist!

If you find some more, please let me know. Ideally I am trying to keep this
code simple and idiot proof though.

## Use

You call it with Pandoc as you would normally with a filter:

```bash
pandoc readme.md --filter highlight.py -o readme.html
```

To pick different colours, adjust the following in the top of `highlight.py`:

```python
symbolother_col  = "#FF920D"
symbolmath_col   = "#197BCE"
symbolnumber_col = "#E32929"
symbolpairs_col  = "#AF18DB"
string_col       = "#008000"
comment_col      = "#888888"
line_col         = "#444444"
```

If you don't want the line numbers to be selectable in HTML, add the following
to your CSS:

```css
.disable{
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}
```

To format your source code, simply use the hinting for the syntax like so:

    ```python
    print("hello")
    ```

If you would like to run Python code (more examples
[here](test/python_executor.md), note the CSS highlighting for the output):

    ```python-exec
    print("hello")
    ```

You can also plot graphs with something like the following:

    ```python-exec
    p = Plott(title = "Test Title")
    p.plot([0, 1, 2, 3], [10, 8, 6, 4])
    print(p.to_uri())
    ```

In fact, any Data URI (output starting with `data:`) will be wrapped in an
image tag if output by the executor.

*Enjoy!*

## Issues

If you see `/usr/bin/env: ‘python’: No such file or directory`, run:

```bash
sudo apt-get install python-is-python3
```

If you see `ModuleNotFoundError: No module named 'pandocfilters'`,
run:

```bash
pip install pandocfilters
```
