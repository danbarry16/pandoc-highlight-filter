#!/bin/bash

pandoc -s --filter highlight.py test/python_executor.md -o test/python_executor.html
# TODO: Add support for PDF.
pandoc -s --filter highlight.py test/python_executor.md -o test/python_executor.pdf
