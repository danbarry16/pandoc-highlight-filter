---
title: Test - Python Executor
header-includes: |
  <style>.python-out{background:#000;color:#FFF;}</style>
---

The following is a normal code block from Python, not designed to run:

```python
print("<b>hello</b>")
```

And the following is another Python block that *is* designed to run
(demonstrating HTML escape characters at the same time):

```python-exec
print("<b>hello</b>")
```

A slightly more complex demo:

```python-exec
def fact(n) :
  if n <= 1 :
    return n
  else :
    return n * fact(n - 1)
print(str(3) + "! = " + str(fact(3)))
```

And we can continue to use the defined functions later:

```python-exec
for x in range(32) :
  print(str(x) + "! = " + str(fact(x)))
```

We can limit the time taken to complete:

```python-exec
import time
while True:
  time.sleep(1)
  print("1 second passed")
```

**But** we can block this timeout using a try-except:

```python-exec
import time
for x in range(15) :
  try :
    time.sleep(1)
  except :
    print("We just swallowed the exception")
  print("1 second passed")
print("We shouldn't see this message, timeout should have occurred")
```

We shouldn't be able to use variables or functions declared outside of the
`exec`:

```python-exec
print("Escape characters: " + str(escape))
```

```python-exec
print(is_unix())
```

We should not be able to override variables of functions declared outside of
the `exec` either:

```python-exec
local_var = 10
print(local_var)
escape = "new value"
print(escape)
```

Now we will check plotting of data:

```python-exec
import math # Import not needed for the graph
p = Plott( # Plot is imported by default
  title = "Math Function Comparison", # Main title
  x_title = "Radians", # X axis title
  y_title = "Amplitude", # Y axis title
  fg = "#FFF", # Foreground colour
  bg = "#222" # Background colour
)
x = list(range(int(math.pi * 20)))
y = []
z = []
m = []
for i in range(len(x)) : # Generate some graph data
  x[i] = float(x[i]) / 10.0
  y += [ math.sin(x[i]) ]
  z += [ math.cos(x[i]) ]
  m += [ math.sin(x[i]) * math.cos(x[i]) ]
p.plot(x, y, "Sine") # Plots are added like this
p.plot(x, z, "Cosine", colour = "#F0F") # The colour can be set
p.plot(x, m) # If no label provided, one gets generated
print(p.to_uri()) # Graph gets printed as Data URI
```

We can also do simple bar charts:

```python-exec
p = Plott( # Plot is imported by default
  title = "Numbers of Pets", # Main title
  x_title = "Type", # X axis title
  y_title = "Count" # Y axis title
)
p.plot(
  [ "Dog", "Cat", "Rabbit", "Hamster", "Fish", "Turtle", "Shark", "T-Rex" ],
  [ 10, 7, 3, 3, 2, 1, 2, 3 ]
) # Bar chart
print(p.to_uri()) # Graph gets printed as Data URI
```
